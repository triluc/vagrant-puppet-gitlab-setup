class {'gitlab_ci_multi_runner':
    nice => '15'
}

gitlab_ci_multi_runner::runner { "puppetrunner":
    gitlab_ci_url => 'http://10.8.0.229/',
    tags          => ['adblockplus', 'puppet'],
    token         => 'RzgpZczqQyeMfWzcjFfy',
    executor      => 'shell',
    run_untagged  => true,
}

file {'/etc/sudoers.d/gitlabrunner':
  ensure => present,
  owner => root,
  group => root,
  mode => '0440',
  content => 'gitlab_ci_multi_runner ALL=(ALL) NOPASSWD: ALL',
}
